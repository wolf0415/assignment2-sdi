import pytest
from module import utils
import os
import pytest
from unittest.mock import patch, mock_open
from module.engine.matching_engine import MatchingEngine


def test_punctuation_scriping(sentence1):
    """ test utils module if punctuation are stripped in a sentence """
    assert utils.strip_punctuation(sentence1) == "Hi John  how are you today "


def test_text_to_words(sentence1):
    assert utils.text_to_words(sentence1) == utils.strip_punctuation(sentence1).split()


def test_if_asked_questions_log_exist():
    """ Check if the file for logging exists """

    log_path = 'asked_questions_log.txt'
    assert os.path.exists(f".\\{log_path}")


def test_availability_of_faq_file():
    """ Test if faq.json exists """

    faq_file = os.path.exists(".\\faq.json")
    assert faq_file


def test_computation_of_similarity_score(matching_engine):
    """ Test if the similarity score are correct """

    string1 = "How are you today?"
    string2 = " How are you today ?"
    assert matching_engine.compute_similarity_score(string1, string2) == 1


def test_text_sanitization(matching_engine):
    """ Test if the sanitization of work correctly """

    sanitized_text = matching_engine.sanitize_text("        How are you today?           ")
    assert sanitized_text == "how are you today".split()


def test_return_question_and_answer(matching_engine):
    """ Test if questions and answer returned correctly """

    user_input = "What day is today?"
    answer = "Monday"
    matching_engine.return_best_match_question_and_answer(user_input)
    assert user_input == matching_engine.question and matching_engine.answer == answer


def test_retrieve_faq_answer_with_index(question_catalogue_access):
    """ Test if the method the answer with the correct index """

    assert question_catalogue_access.retrieve_faq_answer_with_index(0) == "Monday"


def retrieve_faq_with_index(question_catalogue_access):
    assert question_catalogue_access.retrieve_faq_with_index(1) == "What is the weather like today?"


def test_return_wrong_question_and_answer(question_catalogue_access, matching_engine):
    """ test if it will return the wrong answer """

    matching_engine = MatchingEngine(question_catalogue_access)
    user_input1 = "bla bla nbla?"
    matching_engine.return_best_match_question_and_answer(user_input1)
    assert matching_engine.question == "" and matching_engine.answer == ""


def test_retrieve_faq(question_catalogue_access):
    assert type(question_catalogue_access.retrieve_all_faq()) == list


def test_answer_question(question_answering_manager, matching_engine):
    """ test the manager return the tuple of (matching_engine,0) """
    returned_object = question_answering_manager.answer_question("What day is today?")
    assert returned_object == (matching_engine, 0)


def test_answer_wrong_question(question_answering_manager):
    """ test if the manager return tuple of (False,0) """
    returned_object = question_answering_manager.answer_question("Bla bala bla")
    assert returned_object == (False, 0)


def test_write_into_questions_log_file(question_log_access):
    """ Test whether it able to log question into a asked_questions_log.txt """

    file_path = "asked_questions_log.txt"
    content = "Did it snow yesterday?"
    with patch('question_log_access.open', mock_open()) as mocked_file:
        question_log_access.log_question(content)
        mocked_file.assert_called_once_with(file_path, 'a')
        mocked_file().write.assert_called_once_with(content+"\n")
