import pytest
from module.access.question_log_access import QuestionLogAccess
from module.engine.matching_engine import MatchingEngine
from module.access.question_catalogue_access import QuestionCatalogueFileAccess
from module.manager.question_answering_manager import QuestionAnsweringManager



@pytest.fixture(scope="module")
def question_log_access():
    question_log_access = QuestionLogAccess('asked_questions_log.txt')
    return question_log_access


@pytest.fixture(scope="module")
def question_catalogue_access():
    question_catalogue_access = QuestionCatalogueFileAccess('faq.json')
    return question_catalogue_access


@pytest.fixture(scope="module")
def matching_engine(question_catalogue_access):
    matching_engine = MatchingEngine(question_catalogue_access)
    return matching_engine


@pytest.fixture(scope="module")
def sentence1():
    return "Hi John, how are you today?"


@pytest.fixture(scope="module")
def question_answering_manager(matching_engine, question_log_access):
    question_answering_manager = QuestionAnsweringManager(matching_engine, question_log_access)
    return question_answering_manager
