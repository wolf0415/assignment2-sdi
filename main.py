from module.interactive_console_client import InteractiveConsoleClient
from module.engine.matching_engine import MatchingEngine
from module.manager.question_answering_manager import QuestionAnsweringManager
# from question_catalogue_access import QuestionCatalogueFileAccess
from module.access.question_catalogue_access import *
from module.access.question_log_access import QuestionLogAccess


def main(candidates_path, questions_log_path):

    question_catalogue_access = QuestionCatalogueFileAccess(candidates_path)
    matching = MatchingEngine(question_catalogue_access)

    question_log_access = QuestionLogAccess(questions_log_path)
    manager = QuestionAnsweringManager(matching, question_log_access)

    client = InteractiveConsoleClient(manager)
    client.run()


if __name__ == '__main__':
    main("faq.json", "asked_questions_log.txt")
