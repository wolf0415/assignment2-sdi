# ***Project Name: Question Answering System***

# Table of contents
* [About Author](#author)
* [Introduction](#introduction)
* [Things to note](#things-to-note)
* [Technologies Used](#technologies-used)
* [Dependencies](#dependencies)
* [Executing Program](#executing-program)
* [Setup](#setup)
    * [Install pytest](#install-pytest)
* [Run test](#run-test)



## **About Author**<a name="author"></a>

- Name: Jack Kho
- Email: kholy008@mymail.unisa.edu.au

## **Introduction** <a name="introduction"></a>

In this assignment, question answering system will be developed. User will prompt for an input (question). Later on, it will go through comparision between the user input (question) and question in the question-answer pairs.

## Things to note <a name="things-to-note"></a>

- ```conftest.py``` file is storing fixtures for pytest. We don't need to import the ```conftest.py``` into test_components.py as the tests run, it will automatically search for the conftest.py file and import fixtures.
- ```interfaces_of_all_components.py``` file will store all abstract classes that will be used in this Question Answering System

## **Technologies Used**<a name="technologies-used"></a>

- Use python 3.x distribution 
- pytest 

## **Dependencies**<a name="dependencies"></a>

* python must be installed 
* pytest must be installed before testing.
## **Executing Program**<a name="executing-program"></a>

- run main.py

## **Setup** <a name="Setup"></a>

### **Install pytest** <a name="install-pytest"></a>
Try of of these and see whether it works. 
* In **Windows environment**,
    ```
    pip install -r requirements.txt
    python -m pip install -r requirements.txt
    py -m pip install -r requirements.txt
    pip install -U -r requirements.txt --user
    python -m pip install -U -r requirements.txt --user
    py -m pip install -U -r requirements.txt --user
    ```

* In **Linux or UNIX environment**,
    ```
    pip3 install -r requirements.txt 
    pip3 install -U -r requirements.txt --user
    ```
    * If pip is not installed, 
    ```
    sudo apt install python3-pip
    ```

## **Run test** <a name="run-test"></a>

- Run one of these commands if you didn't include python/py as ***ENVIRONMENT VARIABLE***
    ```
    python -m pytest [filename]
    py -m pytest [filename]
    ```
In Linux or UNIX environment, ```python3 -m pytest [filename]```

- Run this command if you included python/py as ***ENVIRONMENT VARIABLE***
    ```
    pytest [filename]
    ```

