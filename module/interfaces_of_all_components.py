import abc


class IQuestionCatalogueAccess(metaclass=abc.ABCMeta):
    """ Abstract class for QuestionCatalogueAccess """

    @abc.abstractmethod
    def read_faq_database(self):
        """ Read question from some form of database """

    @abc.abstractmethod
    def retrieve_all_faq(self):
        """ Retreive question from database """


class IQuestionLogAccess(metaclass=abc.ABCMeta):
    """ Abstract class for QuestionLogAccess """

    @abc.abstractmethod
    def log_question(self):
        pass


class IMatchingEngine(metaclass=abc.ABCMeta):
    """ Matching Engine interface """

    @abc.abstractmethod
    def sanitize_text(self):
        pass

    @abc.abstractmethod
    def compute_similarity_score(self):
        pass

    @abc.abstractmethod
    def return_best_match_question_and_answer(self):
        pass
