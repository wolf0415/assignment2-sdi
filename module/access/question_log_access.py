from module.interfaces_of_all_components import IQuestionLogAccess
import datetime as dt

class QuestionLogAccessAdapter(IQuestionLogAccess):
    """ act as a adapter for different types of logging """

    def log_question(self):
        pass


class QuestionLogAccess(IQuestionLogAccess):
    """ Store asked questions in Asked Question Store """

    def __init__(self, question_log_path: str):
        self.question_log_path = question_log_path

    def log_question(self, question: str) -> None:
        """ log input to ask_question_log.txt """

        with open(self.question_log_path, "a") as out_file:
            out_file.write("question + "  + "\n")
