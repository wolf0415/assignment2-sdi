import json
from module.interfaces_of_all_components import IQuestionCatalogueAccess


class QuestionCatalogueAccessAdapter(IQuestionCatalogueAccess):
    """ Adapter for QuestionCatalogueAccess """

    def __init__(self, connection):
        self.connection = connection  # other db connection initialize here

    def connect_faq_database(self):
        """ process of authentication and authroization of the database initialize here  """

    def retrieve_faq(self):
        """ Retrieve faq in the database """

    def read_faq_database(self):
        """ Start dealing with the database """


class QuestionCatalogueFileAccess(IQuestionCatalogueAccess):
    """ Read and retrieve questions from question catalogue store  """

    def __init__(self, candidate_path: str):
        self.candidate_path = candidate_path
        self.faq_json_file = self.read_faq_database()

    def read_faq_database(self):
        """ read json file """

        with open(self.candidate_path, "r") as faq_file:
            faq = json.load(faq_file)
            return faq

    def retrieve_all_faq(self) -> list:
        """ Retreive all question """

        question_list = [question["question"] for question in self.faq_json_file]
        return question_list

    def retrieve_faq_with_index(self, index_of_faq: int) -> str:
        """ Retrieve questions with index """

        faq = self.faq_json_file[index_of_faq]["question"]
        return faq

    def retrieve_faq_answer_with_index(self, index_of_faq_answer: int) -> str:
        """ Retrieve answer with index """

        faq_answer = self.faq_json_file[index_of_faq_answer]["answer"]
        return faq_answer
