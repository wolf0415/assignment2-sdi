from module.interfaces_of_all_components import *


class QuestionAnsweringManager:
    """ Orchestrates the use case UC02 """

    def __init__(self, matching_engine: IMatchingEngine, question_logging: IQuestionLogAccess):
        self.matching_engine = matching_engine
        self.question_logging = question_logging

    def answer_question(self, question_text: str):
        """ Return matched question and answer """

        self.question_logging.log_question(question_text)
        if self.matching_engine.return_best_match_question_and_answer(question_text):
            return self.matching_engine, 0
        return False, 0
