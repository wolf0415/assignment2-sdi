import random
import module.utils as matching_module  # can test this import to make sure it works
from module.interfaces_of_all_components import *


class MatchingEngine(IMatchingEngine):
    """ Computes the most similiar question answer pair for a given question text """

    def __init__(self, question_catalogue_access: IQuestionCatalogueAccess) -> None:
        self.question_catalogue_access = question_catalogue_access
        self.question = ""
        self.answer = ""

    def sanitize_text(self, text: str) -> list:
        """ sanitize the the question written by the user """

        text_without_punctuation = matching_module.text_to_words(text)
        text_with_all_lower_case = matching_module.words_to_lowercase(text_without_punctuation)
        return text_with_all_lower_case

    def compute_similarity_score(self, user_question: str, stored_faq_question: str) -> float:
        """ compute jacarrd similarity score """

        user_q = self.sanitize_text(user_question)
        faq_q = self.sanitize_text(stored_faq_question)
        return matching_module.jaccard_similarity_score(user_q, faq_q)

    def return_best_match_question_and_answer(self, user_input: str):
        """ Choose which answer and question to return the client """
        score_list = []
        for question in self.question_catalogue_access.retrieve_all_faq():
            # return the score in float (not index of the list)
            score_list.append(self.compute_similarity_score(user_input, question))

        # check if occurance of same score more than 1
        if score_list.count(max(score_list)) > 1:
            if score_list.count(0) == len(score_list):
                return False
            # store index of multiple highest score
            index_post_list = [index_of_score for index_of_score in range(len(score_list)) if score_list[index_of_score] == max(score_list)]
            index_of_question_and_answer = random.choice(index_post_list)
        else:
            # return highest score
            index_of_question_and_answer = score_list.index(max(score_list))
        return self._return_question_and_answer(index_of_question_and_answer)

    def _return_question_and_answer(self,index_of_question_and_answer):
        self.question = self.question_catalogue_access.retrieve_faq_with_index(index_of_question_and_answer)
        self.answer = self.question_catalogue_access.retrieve_faq_answer_with_index(index_of_question_and_answer)
        return self
